<?php
	session_start();
	if ($_SESSION['user']==''){
		header('Location:index.php');
	}
 ?>
<!doctype html>
<html>
<head>
	<?php
		include('meta.php');
		echo $meta; ?>
	<script type="text/javascript">
		ordenestrabajo('','','','','','','');
		cargasectores();
		$(function () {
			  $( function() {
    			$("#filtrofini").datepicker({dateFormat:'dd-mm-yy'});
    			$("#filtroffin").datepicker({dateFormat:'dd-mm-yy'});
    			$("#fechaDeterminacion").datepicker({dateFormat:'dd-mm-yy'});
  			});
        });
	</script>
</head>
<body>
	<nav style="background: #337AB7;" class="navbar navbar-default navbar-fixed-top">
  		<div class="container">
  			<img src="img/logo.png" width="50" height="50" style="float: left;">
  			<span style="float: right;">&nbsp;</span>
  			<span style="float: right;">&nbsp;</span>
  			<span class="glyphicon glyphicon-log-out pointer" id="logout" onclick="exit()" title="Salir" style="float: right; font-size: 23px; padding-top: 15px; color:#FFF;"></span>
  			<span style="float: right;">&nbsp;</span>
  			<span style="float: right;">&nbsp;</span>
  			<span style="float: right; font-size: 16px; padding-top: 15px; color:#FFF;"> <?php echo $_SESSION['user']; ?> </span>
  			<!-- <span class="glyphicon glyphicon-user" style="float: right; font-size: 23px; padding-top: 15px; color:#FFF;"></span> -->

  		</div>
	</nav>
	<div class="container">
		<div class="contdata" style="margin-top: 80px; padding-left: 30px; padding-right: 30px;" >
			<div class="row" style="margin-top: 10px;">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<span class="subtit"  >Panel de Filtros<span>
					<hr class="lineatit"> </hr>
				</div>
			</div>
			<div class="row" style="padding: 10px;">
				<div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
					<div  class="row">
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><select id="filtroestado" class="form-control"> <option selected disabled>Estado</option> <option value="">Todos</option>    <option value="1">Abiertas</option> <option value="0">Cerradas</option></select> </div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><input class="form-control" type="text" id="filtrofini" placeholder="Fecha Alta Ini" /></div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><input class="form-control" type="text" id="filtroffin" placeholder="Fecha Alta Fin" /></div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><input class="form-control" type="text" id="filtroensayo" placeholder="Filtro Ensayo" />   </div>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> <span id="opensector"></span>  </div>
					</div>
					<div style="margin-top: 15px;" class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><input class="form-control" type="text" id="filtromuestra" placeholder="Muestra" /></div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><input class="form-control" type="text" id="filtrocomentario" placeholder="Comentario" /></div>
					</div>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
					<button style="margin-top: 40%;" class="btn btn-primary btn-sm btnfilter center-block"  id="filter" role="button"> Filtrar </button>
				</div>
			</div>
		</div>
		<div class="contdata" style="margin-top: 20px; padding-left: 30px; padding-right: 30px; padding-top: 15px; padding-bottom: 15px;  " >
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<span class="subtit">Ordenes de Trabajo<span>
					<span id="numreg" class="badge" title="Cantidad de Ordenes"></span>
					<hr class="lineatit"> </hr>
				</div>
			</div>
			<div id="resulTableOrdenes" >
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade modalloadresultados" atras=""  data-keyboard="false" data-backdrop="static" id="modalloadresultados" tabindex="-1" role="dialog" aria-labelledby="mymodalloadresultados">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content" id="myModalloadresultadosContent">
					<div class="modal-header">
						<h3 class="modal-title">
							<i class="glyphicon glyphicon-th-list"  aria-hidden="true"></i>&nbsp;Carga de Resultados
							<i style="float:right" class="glyphicon glyphicon-remove pointer" aria-hidden="true" id="closeCargaResul"></i>
						</h3>
					</div>
					<div class="modal-body">
						<div id="listadodeterminaciones">"

						</div>
					</div>
				</div>
			</div>
		</div>

</body>
</html>
