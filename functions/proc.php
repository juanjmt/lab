<?php
	include('funciones.php');
	$lab=new lab;

	$conn_string = "host=localhost port=5432 dbname=laboratorio user=postgres password=12345678 options='--client_encoding=UTF8'";
	$dbcon = pg_connect($conn_string);
 	if(!$dbcon) echo "<p><i>Error de Conexión</i></p>";

 	switch($_POST['acc']){
 		case 'logon':
 			echo $lab->logon($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'permisos':
 			echo $lab->permisos($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'ordenestrabajo':
 			echo $lab->ordenestrabajo($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'determinaciones':
 			echo $lab->determinaciones($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'guardarresultados':
 			echo $lab->guardarresultados($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'cargaensayos':
 			echo $lab->cargaensayos($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'cargasectores':
 			echo $lab->cargasectores($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'modificarResul':
 			echo $lab->modificarResul($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'removeResul':
 			echo $lab->removeResul($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'exit':
 			session_destroy();
 			echo "Sesión finalizada";
 		break;
 		case 'defectoDeterminaciones':
 			echo $lab->defectoDeterminaciones($_POST,$dbcon);
 			pg_close($dbcon);
 		break;
 		case 'CargarSelectMetodos':
 			echo $lab->CargarSelectMetodos($_POST,$dbcon);
 			pg_close($dbcon);
 		break;


 	}



 ?>
