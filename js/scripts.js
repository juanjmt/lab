$(function() {
	var otensayo="";
	var cotio="";
});

function logon(){
	user=$('#user').val();
	pass=$('#pass').val();
	if (user!='' && pass!=''){
		$.ajax({
			type:"POST",
			url:'functions/proc.php',
			data : { 'acc' : 'logon','user':user,'pass':pass },
			success: function(data){
				if (data!='-1'){
					if (Number(data)){
						permisos(user);
					}else{
						alert(data);
					}
				}else{
					toastr.clear();
					toastr.error('<strong>Error...</strong>, los datos de usuario ingresados son incorrectos.');
				}
			}
		});
	}else{
		toastr.warning('Los campos deben estar llenos');
	}
}
function permisos(user){
	$.ajax({
		type:"POST",
		url:'functions/proc.php',
		data : { 'acc':'permisos','user':user,'menu':'MNURESULTADOS','modulo':'ApWeb'},
		success: function(data){
			dataray=data.split('=');
			if (dataray[0]!='-1'){
				window.location.href ='appweb.php';
				$('#user').val('');
				$('#pass').val('');
			}else{
				toastr.clear();
				toastr.error('<strong>Error...</strong>,el usuario ingresado no tiene permisos habilitados en el módulo');
			}

		}

	});
}
function cargaensayos(){
	$.ajax({
		type:"POST",
		url:'functions/proc.php',
		data : { 'acc':'cargaensayos'},
		success: function(data){
			$('#opensayo').html(data);

		}

	});
}
function cargasectores(){
	$.ajax({
		type:"POST",
		url:'functions/proc.php',
		data : { 'acc':'cargasectores'},
		success: function(data){
			$('#opensector').html(data);

		}

	});
}

function ordenestrabajo(filtroestado,filtrofini,filtroffin,filtroensayo,filtrosector,filtromuestra,filtrocomentario){
	$.ajax({
		type:"POST",
		url:'functions/proc.php',
		data : { 'acc':'ordenestrabajo',filtroestado:filtroestado,filtrofini:filtrofini,filtroffin:filtroffin,filtroensayo:filtroensayo,filtrosector:filtrosector,filtromuestra:filtromuestra,filtrocomentario:filtrocomentario},
		success: function(data){
			setTimeout(function(){
				dataray=data.split('||');
				$("#resulTableOrdenes").html(dataray[0]);
				$("#numreg").html(dataray[1]);
				$("#listordenes").bootstrapTable({
					onClickRow: function (row, $element) {
						$('#modalloadresultados').modal();
						otensayo=$element.attr('data-oten');
						cotio=$element.attr('data-cotio');
						FormCargaDeterminaciones(otensayo,cotio);

					}
				});

			}, 500);
		},
		beforeSend:function(data){
			$("#resulTableOrdenes").html('Cargando Información...');
		},
		complete:function(data){
			$('#closeCargaResul').off('click');
			$("#closeCargaResul").click(function(e){
				$('#modalloadresultados').modal('hide');
			});
			$("#filter").off('click');
			$("#filter").click(function(e){
				filtroestado=$('#filtroestado').val();
				filtrofini=$('#filtrofini').val();
				filtroffin=$('#filtroffin').val();
				filtroensayo=$('#filtroensayo').val();
				filtrosector=$('#filtrosector').val();
				filtromuestra=$('#filtromuestra').val();
				filtrocomentario=$('#filtrocomentario').val();
				ordenestrabajo(filtroestado,filtrofini,filtroffin,filtroensayo,filtrosector,filtromuestra,filtrocomentario);

			});


		}
	});
}

function FormCargaDeterminaciones(otensayo,cotio){
	$.ajax({
		type:"POST",
		url:'functions/proc.php',
		data : { 'acc':'determinaciones',otensayo:otensayo,cotio:cotio},
		success: function(data){
			$("#listadodeterminaciones").html(data);
		},
		complete: function(data){
	  		$( function() {
				$("#fechaDeterminacion").datepicker({dateFormat:'dd-mm-yy'});
			});
 			arraycod=$("#listadodeterminaciones option:selected").text();
			arraycod=arraycod.split('-');
			cargarValoresDefecto(otensayo,cotio,arraycod[0],function(){
				loadAllcombox();
			});

			$(".editResul").off('click');
			$(".editResul").click(function(){
				valor=$(this).attr('valor');
				editResul(valor,otensayo,cotio);
			});
			$(".removeResul").off('click');
			$(".removeResul").click(function(){
				valor=$(this).attr('valor');
				removeResul(valor,otensayo,cotio);
			});
			$("#selectdeterminaciones").change(function(){
				//alert("cambio de combo en linea 300");
				arraycod=$("#listadodeterminaciones option:selected").text();
				arraycod=arraycod.split('-');
				$('.selectnormainput').val('');
				$('.selectequipoinput').val('');
				$('.selectuminput').val('');
				$('.selectmetodoinput').val('');
				$('.selectpalabrasinput').val('');
				cargarValoresDefecto(otensayo,cotio,arraycod[0],function(){
					valnorma=$("#selectnorma option:selected").html();
					$('.selectnormainput').val(valnorma);
					valequipo=$("#selectequipo option:selected").html();
					$('.selectequipoinput').val(valequipo);
					valum=$("#selectum option:selected").html();
					$('.selectuminput').val(valum);
					valmetodo=$("#selectmetodo option:selected").html();
					$('.selectmetodoinput').val(valmetodo);
					valpalabras=$("#selectpalabras option:selected").html();
					$('.selectpalabrasinput').val(valpalabras);
				});
			});
			$("#selectmetodo").keyup(function(event){
				$('#Valortypemetodo').show();
				$('#Valortypemetodo').focus();
				if ($('#Valortypemetodo').val().length<=0){
					$('#Valortypemetodo').val(String.fromCharCode(event.which));
				}
				$('#Valortypemetodo').keyup(function(event){

					valorall=$('#Valortypemetodo').val();
					$.ajax({
						type:"POST",
						url:'functions/proc.php',
						data : { 'acc':'CargarSelectMetodos',filtro:valorall},
						success: function(data){
							$("#selectmetodo").html(data);

						}
					});

				});
			});
			$("#selectmetodo").change(function(){
				$('#Valortypemetodo').hide();
				$('#Valortypemetodo').val('');

			});
			//loadCombosBusquedas();
			/* aca tenemos que poner o convertir a cada combo en uno de busqueda */
		}
	});
}
function loadAllcombox(){
	loadCombosBusquedas('selectnorma','selectnormainput',function(){
		loadCombosBusquedas('selectequipo','selectequipoinput',function(){
			loadCombosBusquedas('selectum','selectuminput',function(){
				loadCombosBusquedas('selectmetodo','selectmetodoinput',function(){
					loadCombosBusquedas('selectpalabras','selectpalabrasinput',function(){

					});
				});
			});

		});
	});
}
function loadCombosBusquedas(idselect,classid,callback) {
	//alert('llamo a los combos');
  	$.widget( "custom.combobox", {
		_create: function() {
		  this.wrapper = $( "<span>" )
			.addClass( "custom-combobox" )
			.insertAfter( this.element );

		  this.element.hide();
		  this._createAutocomplete();
		  this._createShowAllButton();
		},
		_createAutocomplete: function() {
		  var selected = this.element.children( ":selected" ),
			value = selected.val() ? selected.text() : "";

		  this.input = $( "<input>" )
			.appendTo( this.wrapper )
			.val( value )
			.attr( "title", "" )
			.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
			.addClass(classid)
			.autocomplete({
			  delay: 0,
			  minLength: 0,
			  source: $.proxy( this, "_source" )
			})
			.tooltip({
			  classes: {
				"ui-tooltip": "ui-state-highlight"
			  }
			});
			//alert(selected.val());
		//$('.ui-autocomplete-input').focus().val('dsadsad');
		  this._on( this.input, {
			autocompleteselect: function( event, ui ) {
			  ui.item.option.selected = true;
			  this._trigger( "select", event, {
				item: ui.item.option
			  });
			},

			autocompletechange: "_removeIfInvalid"
		  });
		},

		_createShowAllButton: function() {
			var input = this.input,
			wasOpen = false;

		  $( "<a>" )
			.attr( "tabIndex", -1 )
			.attr( "title", "Mostrar todas las opciones" )
			.css("height","2.1em")
			.css("margin-top", "-0.2em")
			.tooltip()
			.appendTo( this.wrapper )
			.button({
			  icons: {
				primary: "ui-icon-search"
			  },
			  text: false
			})
			.removeClass( "ui-corner-all" )
			.addClass( "custom-combobox-toggle ui-corner-right" )
			.on( "mousedown", function() {
			  wasOpen = input.autocomplete( "widget" ).is( ":visible" );
			})
			.on( "click", function() {
			  input.trigger( "focus" );
			  $('.ui-widget-content').css('z-index','2000');


			  // Close if already visible
			  if ( wasOpen ) {
				return;
			  }

			  // Pass empty string as value to search for, displaying all results
			  input.autocomplete("search","");
			});
		},

		_source: function( request, response ) {
		  var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
		  response( this.element.children( "option" ).map(function() {
			var text = $( this ).text();
			if ( this.value && ( !request.term || matcher.test(text) ) )
			  return {
				label: text,
				value: text,
				option: this
			  };
		  }) );
		},

		_removeIfInvalid: function( event, ui ) {
			// Selected an item, nothing to do
		  if ( ui.item ) {
			return;
		  }

		  // Search for a match (case-insensitive)
		  var value = this.input.val(),
			valueLowerCase = value.toLowerCase(),
			valid = false;
		  this.element.children( "option" ).each(function() {
			if ( $( this ).text().toLowerCase() === valueLowerCase ) {
			  this.selected = valid = true;
			  return false;
			}
		  });

		  // Found a match, nothing to do
		  if ( valid ) {
			return;
		  }

		  // Remove invalid value
		  this.input
			.val( "" )
			.attr( "title", value + "No coincide con ningun item" )
			.tooltip( "open" );
		  this.element.val( "" );
		  this._delay(function() {
			this.input.tooltip( "close" ).attr( "title", "" );
		  }, 2500 );
		  this.input.autocomplete( "instance" ).term = "";
		},

		_destroy: function() {
		  this.wrapper.remove();
		  this.element.show();
		}
	  });
		/* aqui */
	$("#"+idselect).combobox();
	if(callback) callback();

}

function cargarValoresDefecto(otensayo,cotio,codprod,callback){
	var cargartvalor='';
	var cargarum='';
	var cargarmetodo='';
	$.ajax({
		type:"POST",
		url:'functions/proc.php',
		data : { 'acc':'defectoDeterminaciones',otensayo:otensayo,cotio:cotio,codprod:codprod},
		success: function(data){
			//sconsole.log(data);s
			//$('select option[value="0"]').attr("selected", true);
			$('#limiteCuan').val('');
			$('#resvalor').val('');
			$('#resobser').val('');
			$('#selectpalabras').find('option:selected').removeAttr('selected');

			valhtmlmetodo=$("#selectmetodo").html();
			valhtmltvalor=$("#tipoval").html();
			valhtmlum=$("#selectum").html();
			dataray=data.split('||');
			metodo=dataray[0].split('>');
			cargarmetodo=false;
			cargatvalor=false;
			cargaum=false;
			carganorma=false;
			cargaeq=false;
			met=metodo[0].trim();
			//alert(met);
			$('#selectnorma').find('option:selected').removeAttr('selected');
			$('#selectnorma option').each(function(){
				if (dataray[4]==$(this).val()){
					$("#selectnorma option[value='"+dataray[4]+"']").attr("selected","selected");
					carganorma=false;
					return false;
				}else{
					carganorma=true;
				}

			});
			$('#selectmetodo').find('option:selected').removeAttr('selected');
			$("#selectmetodo option").each(function(){
				if (met===$(this).val()){
					cargarmetodo=false;
					$("#selectmetodo option[value='"+met+"']").attr("selected","selected");
					return false;
				}else{
					cargarmetodo=true;

				}
			});
			$("#tipoval option").each(function(){
				if (dataray[1]==$(this).val()){
					cargartvalor=false;
					$("#tipoval option[value='"+dataray[1]+"']").attr("selected","selected");
					return false;
				}else{
					cargartvalor=true;

				}
			});
			$('#selectum').find('option:selected').removeAttr('selected');
			$("#selectum option").each(function(){
				if (dataray[2]==$(this).val()){
					cargarum=false;
					$("#selectum option[value='"+dataray[2]+"']").attr("selected","selected");
					return false;
				}else{
					cargarum=true;
				}
			});
			dataray[6]=dataray[6].trim();
			$('#selectequipo').find('option:selected').removeAttr('selected');
			$("#selectequipo option").each(function(){
				if (dataray[6]==$(this).val()){
					cargaeq=false;
					$("#selectequipo option[value='"+dataray[6]+"']").attr("selected","selected");
					return false;
				}else{
					cargaeq=true;
				}
			});
			$('#valmax').val(dataray[3]);

			if (cargarmetodo==true){
				$('#selectmetodo').append('<option  value="'+met+'" selected="selected">'+metodo[1]+'</option>');
			}
			if (cargartvalor==true){
				if(dataray[1]==1){
					dataray1valor='Númerico';
					$('#tipoval').append('<option  value="'+dataray[1]+'" selected="selected">'+dataray1valor+'</option>');
				}
				if(dataray[1]==0 || dataray[1]==''){
					dataray1valor='Texto';
					dataray[1]='0';
					$('#tipoval').append('<option  value="'+dataray[1]+'" selected="selected">'+dataray1valor+'</option>');
				}
			}
			if (cargarum==true){
				$('#selectum').append('<option  value="'+dataray[2]+'" selected="selected">'+dataray[2]+'</option>');
			}

			if (carganorma==true){
				$('#selectnorma').append('<option  value="'+dataray[4]+'" selected="selected">'+dataray[4]+'-'+dataray[5]+'</option>');
			}


		},
		complete:function(){
			if (callback){
				callback();
			}
		}
	});
}
function editResul(valor,otensayo,cotio){
	$('#selectmetodoval-'+valor+'').show();
	$('#labelMetodo-'+valor+'').hide();
	$('#ValObten-'+valor+'').hide();
	$('#inputValObten-'+valor+'').show();
	$('#ValMax-'+valor+'').hide();
	$('#inputValMax-'+valor+'').show();
	$('#etimumval-'+valor+'').hide();
	$('#selectmumval-'+valor+'').show();
	$("#editResul-"+valor).hide();
	$("#editResul-"+valor).hide();
	$("#SaveeditResul-"+valor).show();

	$(".SaveeditResul").off('click');
	$(".SaveeditResul").click(function(){
		valor=$(this).attr('valor');
		valobt=$('#inputValObten-'+valor+'').val();
		metodo=$('#selectmetodoval-'+valor+'').val();
		um=$('#selectmumval-'+valor+'').val();
		valmax=$('#inputValMax-'+valor+'').val();
		$.ajax({
			type:"POST",
			url:'functions/proc.php',
			data : { 'acc':'modificarResul',valor:valor,valobt:valobt,metodo:metodo,um:um,valmax:valmax},
			success: function(data){
				datar=data.split('||');
				if (datar[0]=='exito'){
					toastr.info(datar[1]);
					$('#inputValObten-'+valor+'').val('');
					$('#selectmetodoval-'+valor+'').val('');
					$('#selectmumval-'+valor+'').val('');
					$('#inputValMax-'+valor+'').val('');
					$('#selectmetodoval-'+valor+'').show();
					$('#labelMetodo-'+valor+'').show();
					$('#ValObten-'+valor+'').show();
					$('#inputValObten-'+valor+'').hide();
					$('#ValMax-'+valor+'').show();
					$('#inputValMax-'+valor+'').hide();
					$('#etimumval-'+valor+'').show();
					$('#selectmumval-'+valor+'').hide();
					$("#editResul-"+valor).show();
					$("#editResul-"+valor).show();
					$("#SaveeditResul-"+valor).hide();

				}else{
					toastr.error('<strong>Error... </strong>,'+datar[1]);
				}

			},
			complete: function(data){
				$.ajax({
					type:"POST",
					url:'functions/proc.php',
					data : { 'acc':'determinaciones',otensayo:otensayo,cotio:cotio},
					success: function(data){
						$("#listadodeterminaciones").html(data);
						$( function() {
							$("#fechaDeterminacion").datepicker({dateFormat:'dd-mm-yy'});
						});
			 			arraycod=$("#listadodeterminaciones option:selected").text();
						arraycod=arraycod.split('-');
						cargarValoresDefecto(otensayo,cotio,arraycod[0],function(){
							loadAllcombox();
						});

					},
					complete: function(data){
						$(".editResul").off('click');
						$(".editResul").click(function(){
							valor=$(this).attr('valor');
							editResul(valor,otensayo,cotio);
						});
						$(".removeResul").off('click');
						$(".removeResul").click(function(){
							valor=$(this).attr('valor');
							removeResul(valor,otensayo,cotio);
						});
						$('#selectdeterminaciones').change(function(){
							arraycod=$("#listadodeterminaciones option:selected").text();
							arraycod=arraycod.split('-');
							$('.selectnormainput').val('');
							$('.selectequipoinput').val('');
							$('.selectuminput').val('');
							$('.selectmetodoinput').val('');
							$('.selectpalabrasinput').val('');
							cargarValoresDefecto(otensayo,cotio,arraycod[0],function(){
								valnorma=$("#selectnorma option:selected").html();
								$('.selectnormainput').val(valnorma);
								valequipo=$("#selectequipo option:selected").html();
								$('.selectequipoinput').val(valequipo);
								valum=$("#selectum option:selected").html();
								$('.selectuminput').val(valum);
								valmetodo=$("#selectmetodo option:selected").html();
								$('.selectmetodoinput').val(valmetodo);
								valpalabras=$("#selectpalabras option:selected").html();
								$('.selectpalabrasinput').val(valpalabras);
							});
						});




					}
				});

			}
		});
	});

}

function guardarresultados(){
		selectdeterminaciones=$('#selectdeterminaciones').val();
		rayselect=selectdeterminaciones.split('||');
		otensayo_item=rayselect[0];
		otensayo_mvotlab=rayselect[1];
		otensayo_itemcotio=rayselect[2];
		selectmetodo=$('#selectmetodo').val();
		tipoval=$('#tipoval').val();
		resvalor=$('#resvalor').val()+' '+$('#selectpalabras').val();
		selectum=$('#selectum').val();
		valmax=$('#valmax').val();
		resobser=$('#resobser').val();
		fechaana=$('#fechaDeterminacion').val();
		fechaana=fechaana.split('-');
		fechaana=fechaana[2]+'-'+fechaana[1]+'-'+fechaana[0];
		codusuario=$('#codusuario').val();
		selectnorma=$('#selectnorma').val();
		selectequipo=$('#selectequipo').val();
		limiteCuan=$('#limiteCuan').val();
		codusuario='ZEUS';
		//if (selectdeterminaciones!='' && selectmetodo!='' && tipoval!='' && resvalor!='' && selectum!='' && valmax!='' && fechaana!='' && codusuario!='' && selectnorma!='' && selectequipo!='' && limiteCuan!=''){
		if (selectdeterminaciones!='' && resvalor!='' && fechaana!='' && codusuario!=''){
			$.ajax({
				type:"POST",
				url:'functions/proc.php',
				data : { 'acc':'guardarresultados',selectdeterminaciones:selectdeterminaciones,selectmetodo:selectmetodo,tipoval:tipoval,resvalor:resvalor,selectum:selectum,valmax:valmax,resobser:resobser,fechaana:fechaana,codusuario:codusuario,otensayo_item:otensayo_item,otensayo_mvotlab:otensayo_mvotlab,otensayo_itemcotio:otensayo_itemcotio,selectnorma:selectnorma,selectequipo:selectequipo,limiteCuan:limiteCuan},
				success: function(data){
					//alert(data);
					datar=data.split('||');
					if (datar[0]=='exito'){
						toastr.info(datar[1]);

					}else{
						toastr.error('<strong>Error... </strong>,'+datar[1]);
					}
				},
				complete: function(){
					$.ajax({
						type:"POST",
						url:'functions/proc.php',
						data : { 'acc':'determinaciones',otensayo:otensayo,cotio:cotio},
						success: function(data){
							$("#listadodeterminaciones").html(data);

						},
						complete: function(data){
							arraycod=$("#listadodeterminaciones option:selected").text();
							arraycod=arraycod.split('-');
							cargarValoresDefecto(otensayo,cotio,arraycod[0],function(){
								loadAllcombox();
							});
							$(".editResul").off('click');
							$(".editResul").click(function(){
								valor=$(this).attr('valor');
								editResul(valor,otensayo,cotio);
							});
							$(".removeResul").off('click');
							$(".removeResul").click(function(){
								valor=$(this).attr('valor');
								removeResul(valor,otensayo,cotio);
							});
							$('#selectdeterminaciones').change(function(){
								//alert('cambio de combo en linea 608');
 								arraycod=$("#listadodeterminaciones option:selected").text();
								arraycod=arraycod.split('-');
								$('.ui-widget-content').val('');
								cargarValoresDefecto(otensayo,cotio,arraycod[0],function(){
									//loadCombosBusquedas();
								});
							});
						}
					});
				}
			});
		}else{
			toastr.error('<strong>Error... </strong>, todos los campos son obligatorios');
		}

	}
function removeResul(valor,otensayo,cotio){
		$.ajax({
			type:"POST",
			url:'functions/proc.php',
			data : { 'acc':'removeResul',valor:valor},
			success: function(data){
				datar=data.split('||');
				if (datar[0]=='exito'){
					toastr.info(datar[1]);

				}else{
					toastr.error('<strong>Error... </strong>,'+datar[1]);
				}
			},
			complete: function(data){
				$.ajax({
					type:"POST",
					url:'functions/proc.php',
					data : { 'acc':'determinaciones',otensayo:otensayo,cotio:cotio},
					success: function(data){
						$("#listadodeterminaciones").html(data);
						$( function() {
							$("#fechaDeterminacion").datepicker({dateFormat:'dd-mm-yy'});
						});
			 			arraycod=$("#listadodeterminaciones option:selected").text();
						arraycod=arraycod.split('-');
						cargarValoresDefecto(otensayo,cotio,arraycod[0],function(){
							loadAllcombox();
						});

					},
					complete: function(data){
						$(".editResul").off('click');
						$(".editResul").click(function(){
							valor=$(this).attr('valor');
							editResul(valor,otensayo,cotio);
						});
						$(".removeResul").off('click');
						$(".removeResul").click(function(){
							valor=$(this).attr('valor');
							removeResul(valor,otensayo,cotio);
						});
						$('#selectdeterminaciones').change(function(){
							arraycod=$("#listadodeterminaciones option:selected").text();
							arraycod=arraycod.split('-');
							$('.selectnormainput').val('');
							$('.selectequipoinput').val('');
							$('.selectuminput').val('');
							$('.selectmetodoinput').val('');
							$('.selectpalabrasinput').val('');
							cargarValoresDefecto(otensayo,cotio,arraycod[0],function(){
								valnorma=$("#selectnorma option:selected").html();
								$('.selectnormainput').val(valnorma);
								valequipo=$("#selectequipo option:selected").html();
								$('.selectequipoinput').val(valequipo);
								valum=$("#selectum option:selected").html();
								$('.selectuminput').val(valum);
								valmetodo=$("#selectmetodo option:selected").html();
								$('.selectmetodoinput').val(valmetodo);
								valpalabras=$("#selectpalabras option:selected").html();
								$('.selectpalabrasinput').val(valpalabras);
							});
						});


					}
				});

			}
		});

	}
function exit(){
		$.ajax({
		type:"POST",
		url:'functions/proc.php',
		data : { 'acc':'exit'},
		success: function(data){
			window.location="index.php";

		}

	});

	}
