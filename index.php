
<!doctype html>
<html>
<head>
	<?php 
		include('meta.php');
		echo $meta; ?>
</head>

<body id="inicial">
	<div class="loginbox">
		<div class="row tlogin"> <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center"><div  style="margin-bottom:15px;"> Ingresar al Sistema </div> </div>  </div>
		<div class="row"> 
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"> 
				<div class="input-group" style="margin-bottom:15px;">
					<span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
					<input type="text" name="user" id="user" class="form-control" placeholder="Usuario" autocomplete="off">
				</div>
			</div>  
		</div>
		<div class="row"> 
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
				<div class="input-group" style="margin-bottom:15px;">
					<span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
					<input type="password" name="pass" id="pass" class="form-control" placeholder="Contraseña" autocomplete="off">
				</div>
			</div>  
		</div>
		<div class="row"> 
			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 text-center"> 
				<button class="btn btn-primary btn-sm btnlogin"  id="LoginSubmit" role="button" onclick="logon();"> Ingresar</button>
			</div>  
		</div>
	</div>
</body>
</html>